import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image,} from 'react-native';

export default function AboutScreen() {
    return (
        <View style={styles.container}>
                <ScrollView >
                <Text style={styles.tittle}>Tentang Saya</Text>
                <Image source={require('./images/user.png')} style={{width:200,height:200, borderRadius:100, alignSelf:'center'}} />
                <Text style={styles.name}>Apih </Text>
                <Text style={styles.job}>React native Developer</Text>
                <View style={styles.boxProfile}>
                    <Text style={styles.itemTittle}>Portfolio</Text>
                    <View style={styles.skillList}>
                        <View style={{alignItems:"center", margin:5, padding:5,}}>
                            <Image source={require('./images/github_36.png')} style={{width:36,height:36}} />
                            <Text style={styles.item}>@016apih</Text>
                        </View>
                        <View style={{alignItems:"center", margin:5, padding:5,}}>
                            <Image source={require('./images/gitlab_36.png')} style={{width:36,height:36}} />
                            <Text style={styles.item}>@016apih</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.boxProfile}>
                    <Text style={styles.itemTittle}>Hubungi Saya</Text>
                    <View style={styles.skillList}>
                        <Image source={require('./images/facebook_36.png')} style={{width:36,height:36,marginHorizontal:20}} />
                        <Text style={styles.item}>Apih </Text>
                    </View>
                    <View style={styles.skillList}>
                        <Image source={require('./images/instagram_36.png')} style={{width:36,height:36,marginHorizontal:20}} />
                        <Text style={styles.item}>@016_Apih </Text>
                    </View>
                    <View style={styles.skillList}>
                        <Image source={require('./images/twitter_36.png')} style={{width:36,height:36,marginHorizontal:20}} />
                        <Text style={styles.item}>@016Apih </Text>
                    </View>
                </View>
            </ScrollView>
                
        </View>
    )
}

const styles = StyleSheet.create({
    
        container: {
            flex: 1,
            fontFamily:'Roboto',
            color:'#003366',
            alignItems:'center',
            justifyContent:'center',
        },
        logo:{
            width:375,
            height:116,
            marginTop:59,
        },
        formLogin1:{
            marginLeft:25,        
        },
        formLogin:{
            marginBottom:15,
        },
        tittle:{
            alignSelf:"center",
            margin:15,        
            fontSize:24,
            lineHeight:28, 
            color:'#003366',
        },
        textForm:{
            fontSize:16,
            lineHeight:19, 
            color:'#003366',
        },
        inputForm:{
            width:294,
            height:35,
            borderColor: 'gray', 
            borderWidth: 1
        },  
        boxButton:{
            alignItems:'center',
            marginVertical:15,
            alignSelf:"center",
    
        },
        textButton:{
            textAlign:"center", 
            fontSize:24, 
            color:"white", 
            padding:5,
        },
        lightButton : {
            borderWidth:1,
            backgroundColor:"#3ec6ff",
            width:140,
            height:40,
            borderRadius : 15,
            borderColor:"#3ec6ff",
        },
        darkButton : {
            borderWidth:1,
            backgroundColor:'#003366',
            width:140,
            height:40,
            borderRadius : 15,
            borderColor:'#003366',
        }
    
    
});