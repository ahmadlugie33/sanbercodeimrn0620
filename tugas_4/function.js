// var shortcut
conslog = console.log;

// pembatas
conslog("\n===============Tugas 4===============");
conslog("==========Soal 1===========\n");

// function Halo sanber
function teriak() {
    return "Halo Sanbers!";
}

// panggil fungsi teriak();
conslog(teriak());

// pembatas
conslog("\n==========Soal 2==========\n");

// fungsi perkalian
function kalikan(no1, no2) {
    return no1 * no2;
}

// variabel
var num1 = 12;
var num2 = 4;

// variabel fungsi kalikan
var hasilKali = kalikan(num1, num2);

// panggil fungsi hasilKali
console.log(hasilKali);

// pembatas
conslog("\n==========Soal 3==========\n");

// fungsi introduce
function introduce(nama, umur, alamat, hobi) {
    return "Nama saya "+ nama + ", umur saya " + umur + " tahun, alamat saya di " + alamat + ", dan saya punya hobi yaitu " + hobi + "!";
}

// variabel
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"

// var fungsi
var perkenalan = introduce(name, age, address, hobby)

// panggil fungsi introduce
conslog(perkenalan);