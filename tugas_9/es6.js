conslog = console.log;
conslog("\n===============Tugas 9===============");
conslog("==========Soal 1===========\n");

// const golden = function goldenFunction(){
//     console.log("this is golden!!")
//   }

//   golden()
var golden = () => {
    conslog("this is golden!!");
}
golden();

conslog("\n==========Soal 2===========\n");

// const newFunction = function literal(firstName, lastName){
//     return {
//       firstName: firstName,
//       lastName: lastName,
//       fullName: function(){
//         console.log(firstName + " " + lastName)
//         return 
//       }
//     }
//   }

//   //Driver Code 
//   newFunction("William", "Imoh").fullName() 

const newFunction = (firstName, lastName) => {
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: ()=> {
            conslog(firstName + " " + lastName)
            return
        }
    }
}

  //Driver Code 
  newFunction("William", "Imoh").fullName() 

  conslog("\n==========Soal 3===========\n");

  const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

// normal js
// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const destination = newObject.destination;
// const occupation = newObject.occupation;

const { firstName, lastName, destination, occupation, spell} = newObject;
// Driver code
console.log(firstName, lastName, destination, occupation)

conslog("\n==========Soal 4===========\n");

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

// normal js
// const combined = west.concat(east)

// es6
const combined = [...west, ...east]

//Driver Code
console.log(combined)

conslog("\n==========Soal 5===========\n");

const planet = 'earth',
        view = 'glass';
// const 
// normal ja
// var before = 'Lorem ' + view + 'dolor sit amet, ' +  
//     'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
//     'incididunt ut labore et dolore magna aliqua. Ut enim' +
//     ' ad minim veniam'

// es6
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim  ad minim veniam`;
 
// Driver Code
console.log(before) 