import React from "react";
import {NavigationContainer } from '@react-navigation/native';
import { createStackNavigator} from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';

import { SignIn, CreateAccount, Profile, Home } from './Screen';
const Tabs = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const SearchStack = createStackNavigator();
const ProfileStack = createStackNavigator();
const AuthStack =createStackNavigator();

const HomeStackScreen = () => {
  return(
    <HomeStack.Navigator>
      <HomeStack.Screen name="Home" component={Home}/>
      <HomeStack.Screen name="Details" component={Details}
        options={({route})=>({
          title: route.params.name
        })}/>
    </HomeStack.Navigator>
  )
}

const SearchStackScreen = () => {
  return(
    <SearchStack.Navigator>
      <SearchStack.Screen name="Search" component={Search}/>
      <SearchStack.Screen name="Search2" component={Search2}/>
    </SearchStack.Navigator>
  )
}

const ProfileScreen =()=>(
  <ProfileStack.Navigator>
    <ProfileStack.Screen name="Profile" component={Profile} />
  </ProfileStack.Navigator>
)

const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name="Home" component={HomeStackScreen} />
    <Tabs.Screen name="Search" component={SearchStackScreen} />
  </Tabs.Navigator>
)

const Drawer = createDrawerNavigator();


export default () => (
  <NavigationContainer>
<Tabs.Navigator>
  <Tabs.Screen name="Home" component={Home}/>
  <Tabs.Screen name="Profile" component={Profile}/>
</Tabs.Navigator>
    {/* <AuthStack.Navigator>
    
      {/* <AuthStack.Screen name="SignIn" component={SignIn} />
      <AuthStack.Screen name="CreateAccount" component={CreateAccount} options={{ title: 'Create Account'}} /> */}
    {/* </AuthStack.Navigator> */} 
  </NavigationContainer>
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
