import React from 'react';
import { View, Text, ScrollView, TextInput,Button, TouchableOpacity, StyleSheet } from 'react-native';

export default function LoginScreen(){
    return(
        <View style={styles.container}>
            <Image source={require('./images/logo.png')} style={styles.logo} />
            <View style={styles.formLogin1}>
            <Text style={styles.tittle}>Login </Text>  
            <View style={styles.formLogin}>
                <Text style={styles.textForm}>Username / Email </Text>
                <TextInput style={styles.inputForm} 
                    onChangeText={text => onChangeText(text)} />
            </View>
            <View style={styles.formLogin}>
                <Text style={styles.textForm}>Password</Text>
                <TextInput style={styles.inputForm} 
                    onChangeText={text => onChangeText(text)} />
            </View>
            <View style={styles.boxButton}>
                <TouchableOpacity style={styles.lightButton}>
                    <Text style={styles.textButton}>Masuk</Text>
                </TouchableOpacity>
                <Text style={{alignSelf:"center",margin:5, }}>Atau </Text>
                <TouchableOpacity style={styles.darkButton}>
                    <Text style={styles.textButton}>Daftar ?</Text>
                </TouchableOpacity>
            </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        fontFamily:'Roboto',
        color:'#003366',
        alignItems:'center',
        justifyContent:'center',
    },
    logo:{
        width:375,
        height:116,
        marginTop:59,
    },
    formLogin1:{
        marginLeft:25,        
    },
    formLogin:{
        marginBottom:15,
    },
    tittle:{
        alignSelf:"center",
        margin:15,        
        fontSize:24,
        lineHeight:28, 
        color:'#003366',
    },
    textForm:{
        fontSize:16,
        lineHeight:19, 
        color:'#003366',
    },
    inputForm:{
        width:294,
        height:35,
        borderColor: 'gray', 
        borderWidth: 1
    },  
    boxButton:{
        alignItems:'center',
        marginVertical:15,
        alignSelf:"center",

    },
    textButton:{
        textAlign:"center", 
        fontSize:24, 
        color:"white", 
        padding:5,
    },
    lightButton : {
        borderWidth:1,
        backgroundColor:"#3ec6ff",
        width:140,
        height:40,
        borderRadius : 15,
        borderColor:"#3ec6ff",
    },
    darkButton : {
        borderWidth:1,
        backgroundColor:'#003366',
        width:140,
        height:40,
        borderRadius : 15,
        borderColor:'#003366',
    }

});