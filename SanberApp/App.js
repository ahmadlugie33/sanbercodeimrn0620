import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import Navigation from './Tugas/Tugas15/index'
// import ToDo from './Tugas/Tugas14/App'
// import Component from './Latihan/Component/Component';
// import YoutubeUI from './Tugas/Tugas12/App'
// import Styling from './Latihan/styling/index'
// import Toggle from './Latihan/styling/Toggle'
// import Login from './Tugas/Tugas13/LoginScreen'
// import LoginScreen from './Tugas/Tugas13/LoginScreen';
// import AboutScreen from './Tugas/Tugas13/AboutScreen';

export default function App() {
  return (
    // <AboutScreen/>
    <Navigation/>
    // <LoginScreen/>
  // <ToDo/>
    // <YoutubeUI/>
    // <Styling/>
    // <Toggle/>
    // <Component />
    // <View style={styles.container}>
    //   <Text>Open up App.js to start working on your app!</Text>
    //   <StatusBar style="auto" />
    // </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
// import React from 'react';
// import { StyleSheet, Text, View } from 'react-native';

// export default function App() {
//   return (
//     <View style={styles.container}>
//       <Text>To share a photo from your phone with a friend, just press the button below!</Text>
//     </View>
//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });