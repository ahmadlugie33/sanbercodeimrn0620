conslog = console.log;
conslog("\n===============Tugas 6===============");
conslog("==========Soal 1===========\n");
conslog("==========Release 0===========\n");

class Animal {
    // Code class di sini
    constructor(name) {
        this.name = name;
        this.legs = 4;
        this.cold_blooded = false;
    }

}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

conslog("==========Release 0===========\n");

// Code class Ape dan class Frog di sini

class Ape extends Animal {
    constructor(name) {
        super(name);
        this.suara = "Auooo";
        this.legs = 2;
    }
    yell() {
        return conslog(this.suara);
    }
}
class Frog extends Animal {
    constructor(name) {
        super(name);
        this.lompat = "hop hop";
        // this.legs = 4;
    }
    jump() {
        return conslog(this.lompat);
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

conslog("==========Soal 2===========\n");

class Clock {
    constructor({ template }) {
        this.template = template;
        // this.date = new Date();
        // this.hours = this.date.getHours();
        // this.mins = this.date.getMilliseconds();
        // this.secs = this.date.getSeconds();
        // this.output = template.replace()
        //     .replace('h', hours)
        //     .replace('m', mins)
        //     .replace('s', secs);
    }
    render() {
        // if (this.hours < 10) this.hours = '0' + this.hours;
        // if (this.mins < 10) this.mins = '0' + this.mins;
        // if (this.secs < 10) this.secs = '0' + this.secs;
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = this.template.replace('h', hours).replace('m', mins).replace('s', secs);

        console.log(output);
    }
    stop() {
        clearInterval(this.timer)
    }
    start() {
        this.render();
        setInterval(() => this.render(), 1000)
    }
}

var timer;
var clock = new Clock({ template: 'h:m:s' });
clock.start(); 