// ===============================================Jawaban 1======================================== \\

console.log();
console.log("==========================Soal 1==========================");
console.log("======================Looping While=======================");

// variabel
var loop1 = " I love coding";
var loop2 = " I will become a mobile developer";
var number = 20;
var i = 1;
var j = 1;

// increase
console.log("=====================Looping Pertama======================");
console.log();

function naik() {
    while (i <= number) {
        if (i % 2 == 0) {
            console.log(i + " - " + loop1);
            i++;
        } else {
            i++;
        }
    }
}


// jawaban
// panggil fungsi naik
naik();

// pembatas 
// turun decreasing
console.log();
console.log("======================Looping Kedua=======================");
console.log();

// fungsi turun / decreasing
function turun() {
    while (number >= j) {
        // penentuan ganjil genap
        if (number % 2 == 0) {
    
            // jawaban looping
            console.log(number + " - " + loop2);
            number--;
        }
        
        number--;
    }
}

// jawaban
// panggil fungsi turun
turun();

// soal 2
console.log();
console.log("==========================Soal 2==========================");
console.log("=======================Looping For========================");
console.log();

// variabel
var number2 = 20
var ganjil = "Santai";
var genap = "Berkualitas";
var kelipatan3 = "I Love Coding";

// function looping for
function loopingFor() {
    for (let h = 1; h < number2 + 1; h++) {
        if (h % 2 == 0 ) {
            console.log(h + " - " + genap);
        }else if(h % 3 == 0){
            console.log(h + " - " + kelipatan3);
        }else {
            console.log(h + " - " + ganjil);
        }
        
    }
    
}

// jawaban
//  Panggil fungsi Loopng For
loopingFor();

console.log();
console.log("==========================Soal 3==========================");
console.log("====================Persegi Panjang #=====================");
console.log();

// variabel
var panjang = 8;
var lebar = 4;
var pgr1 = "";

// index
var p1 = 0;
var p2 = 0;

// fungsi Persegi Panjang
function persegiPanjang() {
    do {
        pgr1 += "#";
        p1++;
    }
    while (p1 < panjang) {
        while (p2 < lebar) {   
            console.log(pgr1);
            p2++;
        }
        
    }
}

// jawabam
// Panggil Fungsi Persegi Panjang
persegiPanjang();

console.log();
console.log("==========================Soal 4==========================");
console.log("=========================Tangga #=========================");
console.log();

// variabel
var tangga = 7;
var pgrtangga1="";

// fungsi tangga 
function buatTangga() {
    for (let t = 0; t < tangga; t++) {
        pgrtangga1 += "#";
        console.log(pgrtangga1); 
    }
}

// jawaban
// Panggil Fungsi membuat Tangga
buatTangga();

console.log();
console.log("==========================Soal 5==========================");
console.log("======================Papan Catur #=======================");
console.log();

// variabel
var caturGanjil = "";
var caturGenap = "";
var sisi1 = 4;
var sisi2 = 8;

// index
var s1 = 0;
var s2 = 0;

// fungsi papan catur
function papanCatur() {
    do {
        caturGanjil += "# ";
        caturGenap += " #";
        s1++;
    }
    while (s1 < sisi1) {
        while (s2 < sisi2) {
            if (s2 % 2 == 0) {
                console.log(caturGenap);
                s2++;
            } else {
                console.log(caturGanjil);
                s2++;
            }  
        }
        
    }
}

// jawaban
// Panggil fungsi papan catur
papanCatur();
