// var shortcut
conslog = console.log;

// pembatas
conslog("\n===============Tugas 6===============");
conslog("==========Soal 1===========\n");

var now = new Date()
var thisYear = now.getFullYear() // 2020 (tahun sekarang)

function arrayToObject(array) {
    orang = ""
    obj = {}
    // Code di sini 
    if (array == null || array == "") {
        array = "\"\"";
        // return conslog(array);
    } else {
        // var bio;
        for (let i = 0; i < 2; i++) {
            // const element = arrayay[index];
            var umur = array[i, i][i, 3];
            if (umur <= thisYear) {
                umur = thisYear - umur;
            } else {
                umur = "Invalid Birth Year";
                obj.age = umur;
                // return conslog(array);
            }
            obj = {
                firstName: array[i, i][i, 0],
                lastName: array[i, i][i, 1],
                gender: array[i, i][i, 2],
                age: umur
            }

            var namaPanjang = obj.firstName + " " + obj.lastName;
            var noUrut = i + 1;
            orang = orang + noUrut + ". " + namaPanjang + ": {\n firstName: \"" +
                obj.firstName + "\", \n lastName: \"" +
                obj.lastName + "\", \n gender: \"" +
                obj.gender + "\", \n age: \"" +
                obj.age + "\"\n}\n";
        }
        conslog(orang);
    }
    // conslog(bio);
}


// arr = arr.map(function (index) {
//     // namaPanjang = index[0] +" " + index[1];
//     namaPanjang = index[0] + index[1];
//     umur = index[3];
//     if (umur <= thisYear) {
//         umur = thisYear - umur;
//         umur = umur.toString();
//     } else {
//         umur = "Invalid Birth Year";
//     }
//     return {
//         firstName: index[0],
//         lastName: index[1],
//         gender: index[2],
//         age: umur,
//         // namaPanjang: function() {
//         //     return this.firstName + " " + this.lastName;
//         // }
//         // nama: namaPanjang
//     }
// }
// )

// }return conslog(arr);
// namaPanjang = firstName + " " + lastName;
// namaPanjang = arr.firstName +" "+ arr.lastName;




// Driver Code
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people);
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case 
arrayToObject([]) // ""


conslog("\n===============Tugas 6===============");
conslog("==========Soal 2===========\n");

function shoppingTime(memberId, money) {
    // you can only write your code here!
    // saldoyg akan berubah
    var saldo = money;
    // nyimpan array listPurchased
    var temp = [];
    var keranjang = [];
    var listPurchased = [];
    listPurchased = [["Sepatu brand Stacattu", 1500000], ["Baju brand Zoro", 500000], ["Baju brand H&N", 250000], ["Sweater brand Uniklooh", 175000], ["Casing Handphone", 50000]];
    if (memberId == null || memberId == "") {
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    } else if (saldo < 50000) {
        return "Mohon maaf, uang tidak cukup";
    } else {


        for (let i = 0; i < listPurchased.length; i++) {
            if (saldo >= listPurchased[i][1]) {
                // keranjang[i] = listPurchased[i][0];
                // listPurchased.push(keranjang[i][0]);
                keranjang.push(listPurchased[i][0]);
                saldo = saldo - listPurchased[i][1];
            }
        }
        Object = {
            "memberId": memberId,
            "money": money,
            "listPurchased": keranjang,
            "changeMoney": saldo
        }
        temp.push(Object);
    }
    return temp;
    // var list = "";
    // if (money < objShop.value) 
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

conslog("\n===============Tugas 6===============");
conslog("==========Soal 3===========\n");

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var array = [];

    //your code here
    if (arrPenumpang == "[]") {
        return "[]";
    } else {

        for (var i = 0; i < arrPenumpang.length; i++) {

            var berangkat = rute.indexOf(arrPenumpang[i][1]);
            var tujuan = rute.indexOf(arrPenumpang[i][2]);
            var biaya = tujuan - berangkat;
            biaya = biaya * 2000;
            var angkot = {
                penumpang: arrPenumpang[i][0],
                naikDari: arrPenumpang[i][1],
                tujuan: arrPenumpang[1][2],
                bayar: biaya
            }
            //    array = array.push(angkot) jadi error
            array.push(angkot)
            
        }
        // conslog(angkot);
        // return arrPenumpang
        return array;
    }

}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]