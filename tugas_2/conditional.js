
// ===============================================Jawaban 1======================================== \\
console.log();
console.log("==========================Soal 1==========================");
console.log();


var nama;
var peran;
// var selamatDatang = "Selamat datang di Dunia Werewolf, " + nama;
// var salamPeran = "Halo " + peran + " " + nama + ", ";
var peranPenyihir = "kamu dapat melihat siapa yang menjadi werewolf!";
var peranGuard = "kamu akan membantu melindungi temanmu dari serangan werewolf.";
var peranWerewolf = "Kamu akan memakan mangsa setiap malam!";

function ww(nama, peran) {
    var selamatDatang = "Selamat datang di Dunia Werewolf, " + nama;
    var salamPeran = "Halo " + peran + " " + nama + ", ";
    if (nama == null || nama == "") {
        console.log("Nama harus diisi!");
    } else if (peran == "Penyihir" || peran == "penyihir") {
        nama;
        console.log(selamatDatang);
        console.log(salamPeran + peranPenyihir);

    } else if (peran == "Guard" || peran == "guard") {
        nama;
        console.log(selamatDatang);
        console.log(salamPeran + peranGuard);
    } else if (peran == "Werewolf" || peran == "werewolf") {
        nama;
        console.log(selamatDatang);
        console.log(salamPeran + peranWerewolf);
    }
    else {
        console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!");
    }

}

// Output untuk Input nama = '' dan peran = ''
console.log("============== nama = '' dan peran = '' ==============");
ww("", "");
console.log();

//Output untuk Input nama = 'John' dan peran = ''
console.log("============== nama = 'John' dan peran = '' ==============");
ww("John", "");
console.log();

//Output untuk Input nama = 'Jane' dan peran 'Penyihir'
console.log("============== nama = 'Jane' dan peran = 'Penyihir' ==============");
ww("Jane", "Penyihir");
console.log();

//Output untuk Input nama = 'Jenita' dan peran 'Guard'
console.log("============== nama = 'Janita' dan peran = 'Guard' ==============");
ww('Janita, Guard');
console.log();

//Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
console.log("============== nama = 'Junaedi' dan peran = 'Werewolf' ==============");
ww('Junaedi', 'Werewolf');
console.log();

//Output untuk Input nama = 'Lugie' dan peran 'Penyihir'
console.log("============== nama = 'Lugie' dan peran = 'Penyihir' ==============");
ww('Lugie', 'Penyihir')
console.log();

// ================================================soal 2========================================= \\
console.log();
console.log("==========================Soal 2==========================");
console.log("========================Switch Case=======================")
console.log();
// Input
// InputEvent
var tanggal;
var bulan;
var tahun;

// ===============================================Jawaban 2======================================== \\

var tanggalStr = String(tanggal);
var bulanStr = String(bulan);
var tahunStr = String(tahun);
//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945'; 

function kalendar(tanggal, bulan, tahun) {
    // // conditional decision if else tanggal
    // if (0 <= tanggal <= 31) {
    //     tanggalStr = tanggal;
    //     // decision switch case bulan
    //     // kondisi decision if else tahun

    //     if (1900 < tahun < 2200) {
    //         tahunStr = tahun;
    //     } else {
    //         tahunStr = 'Melewati Batas Tahun';
    //     }
    // } else {
    //     tanggalStr = "Tidak Ada Tanggal Tersebut!";
    // }
    switch (true) {
        case (0 > tanggal || tanggal > 31):{
            console.log("Tidak Ada Tanggal Tersebut!");
            break;
        }
        case (1900 > tahun || tahun > 2200):{
            console.log("Melewati Batas Tahun");
            break;
        }
        case (1 > bulan || bulan > 12): {
            console.log("Tidak Ada Bulan Tersebut!");
            break;
        }
        default:
            switch (bulan) {
                case 1: {
                    bulanStr = 'Januari';
                    break;
                }
                case 2: {
                    bulanStr = 'Februari';
                    break;
                }
                case 3: {
                    bulanStr = 'Maret';
                    break;
                }
                case 4: {
                    bulanStr = 'April';
                    break;
                }
                case 5: {
                    bulanStr = 'Mei';
                    break;
                }
                case 6: {
                    bulanStr = 'Juni';
                    break;
                }
                case 7: {
                    bulanStr = 'Juli';
                    break;
                }
                case 8: {
                    bulanStr = 'Agustus';
                    break;
                }
                case 9: {
                    bulanStr = 'September';
                    break;
                }
                case 10: {
                    bulanStr = 'Oktober';
                    break;
                }
                case 11: {
                    bulanStr = 'November';
                    break;
                }
                case 12: {
                    bulanStr = 'Desember';
                    break;
                }
                default: 
                break;
            }
            console.log(tanggal + " " + bulanStr + " " + tahun);
            break;
    }

}



// Output atau hasil
kalendar(18, 11, 1999);
kalendar(32, 12, 2000);
kalendar(23, 40, 2000);
kalendar(23, 11, 3000);

