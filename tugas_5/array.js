// var shortcut
conslog = console.log;

// pembatas
conslog("\n===============Tugas 5===============");
conslog("==========Soal 1===========\n");

// fungsi range
function range(startNum, finishNum) {
    // variabel Array
    var angka = [];

    // decision
    if (startNum == null || startNum == "" || finishNum == null || finishNum == "") {
        // menjadikan nilai null menjadi -1
        angka = -1
        // mengembalikan nilai angka
        return angka;
    } else if (startNum > finishNum) {
        // perulangan untuk menambah array descending
        for (let indexRange = startNum; indexRange >= finishNum; indexRange--) {
            // menambah nilai pada array angka[]
            angka.push(indexRange);
            // return angka
        }
        // mengembalikan nilai angka
        return angka;
    } else {
        // perulangan untuk menambah array ascending
        for (let indexRange = startNum; indexRange <= finishNum; indexRange++) {
            // menambah nilai pada array angka[]
            angka.push(indexRange);
        }
        // mengembalikan nilai angka
        return angka;
    }

}

// hasil
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

// pembatas
conslog("\n===============Tugas 5===============");
conslog("==========Soal 2===========\n");

// fungsi range with step
function rangeWithStep(startNum, finishNum, step) {
    // variabel Array
    var angka = [];

    // decision
    if (startNum == null || startNum == "" || finishNum == null || finishNum == "") {
        // menjadikan nilai null menjadi -1
        angka = -1;
        // mengembalikan nilai angka
        return angka;

    } else if (startNum > finishNum) {
        // perulangan untuk menambah array descending
        for (let indexRange = startNum; indexRange >= finishNum;) {

            // menambah nilai pada array angka[]
            angka.push(indexRange);
            // selisih
            indexRange = indexRange - step;

        }
        // mengembalikan nilai angka
        return angka;

    } else {
        // perulangan untuk menambah array ascending
        for (let indexRange = startNum; indexRange <= finishNum;) {

            // menambah nilai pada array angka[]
            angka.push(indexRange);
            // selisih
            indexRange = indexRange + step;

        }
        // mengembalikan nilai angka
        return angka;
    }
}

// hasil
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

// pembatas
conslog("\n===============Tugas 5===============");
conslog("==========Soal 3===========\n");
function sum() {
    var angka = 0;
    return angka;
}
function sum(startNum, finishNum, step) {
    // variabel Array
    var angka = [];

    // decision
    if (startNum != null && finishNum == null) {
        // if (finishNum == null) {
        // angka = 0;
        angka = startNum;
        // mengembalikan nilai angka
        return angka;

    } else if (startNum > finishNum) {
        // perulangan untuk menambah array descending
        for (let indexRange = startNum; indexRange >= finishNum;) {

            // menambah nilai pada array angka[]
            angka.push(indexRange);
            // selisih
            if (step == null) {
                step = 1;
                indexRange = indexRange - step;
            } else {
                indexRange = indexRange - step;
            }

        }
        // mengembalikan nilai angka
        return angka.reduce(function (a, b) {
            return a + b;
        }, 0);

    } else if (startNum < finishNum) {
        // perulangan untuk menambah array ascending
        for (let indexRange = startNum; indexRange <= finishNum;) {

            // menambah nilai pada array angka[]
            angka.push(indexRange);
            // selisih
            if (step == null) {
                step = 1;
                indexRange = indexRange + step;
            } else {
                indexRange = indexRange + step;
            }

        }
        // mengembalikan nilai angka
        return angka.reduce(function (a, b) {
            return a + b;
        }, 0);
    }

    else {
        angka = 0;
        return angka;
    }
}

// hasil
console.log(sum(1, 10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15, 10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)) // 1
console.log(sum())

conslog("\n===============Tugas 5===============");
conslog("==========Soal 4===========\n");

function dataHandling() {
    for (let i = 0; i < input.length; i++) {
        var array = input[i];

        conslog("\n Nomor ID: " + array[0] +
            "\n Nama Lengkap: " + array[1] +
            "\n TTL: " + array[3] +
            "\n Hobi " + array[4]);
    }
}

//contoh input
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

// hasil
dataHandling();

// pembatas
conslog("\n===============Tugas 5===============");
conslog("==========Soal 5===========\n");

// fungsi
function balikKata(str) {
    // string jadi char
    var potong = str.split('');
    // urutannya char dibalik
    var balik = potong.reverse();
    // digabunglagi
    var gabung = balik.join('');
    // mengembalikan nilai gabung
    return gabung;
}

// hasil
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

// pembatas
conslog("\n===============Tugas 5===============");
conslog("==========Soal 5===========\n");



function dataHandling2(input) {
    // var input[];

    nama = input.splice(1, 1, "Roman Alamsyah Elsharawy");
    // var bulan = input[3];
    input = input.splice(0, 4);
    tambah = input.push("Pria", "SMA Internasional Metro");

    conslog(input);
    var tanggal = input[3].split("/")
    var ftanggal = tanggal.join("-");
    // tanggal = split(",");
    // bulan = 
    var bulan = tanggal[1];
    bulan= parseInt(bulan);
    var bulanStr = "";
    switch (bulan) {
        case 1: {
            bulanStr = 'Januari';
            break;
        }
        case 2: {
            bulanStr = 'Februari';
            break;
        }
        case 3: {
            bulanStr = 'Maret';
            break;
        }
        case 4: {
            bulanStr = 'April';
            break;
        }
        case 5: {
            bulanStr = 'Mei';
            conslog(bulanStr);
            break;
        }
        case 6: {
            bulanStr = 'Juni';
            break;
        }
        case 7: {
            bulanStr = 'Juli';
            break;
        }
        case 8: {
            bulanStr = 'Agustus';
            break;
        }
        case 9: {
            bulanStr = 'September';
            break;
        }
        case 10: {
            bulanStr = 'Oktober';
            break;
        }
        case 11: {
            bulanStr = 'November';
            break;
        }
        case 12: {
            bulanStr = 'Desember';
            conslog(bulanStr);
            break;
        }
        default:
            break;
    }
    
    // var ftanggal = tanggal;
    var urut = tanggal.sort(function(a, b){return b - a});
   
    conslog(urut);
    
    
    conslog(ftanggal);
    var batas = input[1].split("");
    var irisan = batas.slice(0,15);
    var gabungNama = irisan.join("")
    conslog(gabungNama);
        // 



    // for (let i = 0; i < input.length; i++) {
    //     var array = input[i];
    //     // Array[0] = id;
    //     // array[1] = nama;
    //     conslog("\n Nomor ID: " + array[0] +
    //         "\n Nama Lengkap: " + array[1] +
    //         "\n TTL: " + array[3] +
    //         "\n Hobi " + array[4]);
    // }
    // // nama = nama.split(" ");
    // // nama = nama.splice(5, 0, inputNama);
    // // nama = nama.join(" ");
    // // nama = input.slice(1, 2);
    // // nama = nama.splice(1, 0, "Elsharawy");
    // // nama = nama.join(" ");

    // var nama = input[1];
    // 
    // var array;
    // conslog("\n Nomor ID: " + array(0) +
    //     "\n Nama Lengkap: " + array(1) +
    //     "\n TTL: " + array(2) +
    //     "\n Hobi " + array(3));

}



var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
// var namaLengkap = input.splice(1, 1, "Roman Alamsyah Elsharawy");
dataHandling2(input);




// dataHandling2(input);
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */